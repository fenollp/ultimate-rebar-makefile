all: compile
.PHONY: all

compile: rebar deps
	./rebar update-deps compile
.PHONY: compile

deps: rebar
	./rebar get-deps compile

clean: rebar
	./rebar clean
	rm -rf ebin .eunit
.PHONY: clean

distclean: clean
	rm -rf deps
.PHONY: distclean

debug: test
	erl -pa ebin -pa .eunit -pa deps/*/ebin
.PHONY: debug

test: rebar
	./rebar skip_deps=true eunit
.PHONY: test

DIALYZER_PLT = .dialyzer_plt

dialyze: $(DIALYZER_PLT) ebin
	dialyzer -r src deps/*/src --src --plt $<
.PHONY: dialyzer

$(DIALYZER_PLT):
	dialyzer --build_plt --output_plt $@ --apps erts kernel stdlib

doc: rebar
	./rebar doc

rebar:
	git clone git://github.com/rebar/rebar.git rebar.d
	cd rebar.d && ./bootstrap
	mv rebar.d/rebar $@
	rm -rf rebar.d/
